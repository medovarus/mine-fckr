export type Coords = {
  x: number;
  y: number;
}

export type CoordsStr = {
  x: string;
  y: string;
}

export type FieldObj = {
  hasMine: boolean, // Флаг наличия мины в ячейке
  minesAround: number | null, // Количество мин вокруг ячейки. Берется из карты mineMap и наполняется по мере открытия
  opened: boolean, // Визуально открытая ячейка и препятствие для повторных проверок ячеек
  checked: boolean, // Обработанная ячейка поля. Если true, то ячейка не появится в списке проверяемых ячеек
  playerFlag: boolean, // Отметка мины
  cellWithHelp: boolean, // Показываем подсказку для этой ячейки
};

export enum Difficulty {
  low = '0.1',
  normal = '0.3',
  high = '0.5',
}

export type FieldY = {
  [key: string]: FieldObj;
};

export type FieldMap = {
  [key: string]: FieldY;
};

export type MineMap = {
  [key: string]: {
    [key: string]: number;
  }
}
