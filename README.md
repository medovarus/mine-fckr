# mine-fckr

Simple minesweeper on VueJS. You can play it on [medovarus.gitlab.io/mine-fckr/](https://medovarus.gitlab.io/mine-fckr/)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
